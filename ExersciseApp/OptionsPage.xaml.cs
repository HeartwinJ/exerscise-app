﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ExersciseApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OptionsPage : Page
    {
        public OptionsPage()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //PushUp
            this.Frame.Navigate(typeof(PushUpPage), null);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            //Bridge
            this.Frame.Navigate(typeof(BridgePage), null);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            //Crunches
            this.Frame.Navigate(typeof(CrunchesPage), null);
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            //HomePage Button
            this.Frame.Navigate(typeof(MainPage), null);
        }
    }
}
