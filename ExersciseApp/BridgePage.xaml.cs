﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ExersciseApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BridgePage : Page
    {
        public int target = 0;
        public int count = 0;

        public BridgePage()
        {
            this.InitializeComponent();
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen;
            DisplayInformation.AutoRotationPreferences = Windows.Graphics.Display.DisplayOrientations.Portrait;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(OptionsPage), null);
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //target = Convert.ToInt32(textBox.Text);
        }

        DispatcherTimer dt = new DispatcherTimer();
        public void button1_Click(object sender, RoutedEventArgs e)
        {
            dt.Interval = new TimeSpan(00, 0, 1);
            bool enabled = dt.IsEnabled;
            dt.Tick += new EventHandler<object>(dt_Tick);            

            target = Convert.ToInt32(textBox.Text);
            count = 0;

           dt.Start();
        }

        //DispatcherTimer Tick
        public void dt_Tick(object sender, object e)
        {
            if (count != target)
            {
                count++;
                textBlock2.Text = Convert.ToString(count);
            }
            else
            {
                dt.Stop();
                count = 0;
                textBlock2.Text = Convert.ToString(count);
                this.Frame.Navigate(typeof(BridgePage), null);
            }
        }
    }
}
